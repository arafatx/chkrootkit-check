#!/bin/sh
#By Arafat Ali (MaXi32) - Chkrootkit check for warning or infected
#| TODO 1) Check update - No builtin update
#| TODO 2) Check level permission to run this script
#|

/bin/echo "======================================"
/bin/echo "[chkrootkit] is checking system..."

#Global variables
MYHOSTNAME=`/bin/hostname`
MYEMAIL="webmaster@sofibox.com"
MYFILENAME="chkrootkit-report.txt"
GREPRESULT="chkrootkit-grep-result.txt"
WARNING_STATUS="N/A"

#Check if /tmp/$MYFILENAME and GREPRESULT exist, if yes delete them.

if [[ -f /tmp/$MYFILENAME ]] ; then
/bin/rm -rf /tmp/$MYFILENAME
/bin/rm -rf /tmp/$GREPRESULT
/bin/echo "[WARNING] Log files exist in the /tmp/ directory/ and has been removed to restart the log status" > /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME
#/bin/echo "[WARNING] Log file exist and deleted"
else
/bin/echo "[OK] No existing log files were identified. Creating new log files in /tmp/" > /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME
#/bin/echo "[OK] No log file exist"
fi
/bin/echo "Please wait..."
/bin/echo "Chkrootkit checked on `date`" >> /tmp/$MYFILENAME
/usr/local/chkrootkit/chkrootkit >> /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME
/bin/echo "*******************DONE CHECKING*****************" >> /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME
/bin/echo "================= WARNING NOTICE ================" >> /tmp/$MYFILENAME

if (grep -e "Warning" -e "INFECTED" -e "were found" /tmp/$MYFILENAME >> /tmp/$GREPRESULT) then
WARNING_STATUS="WARNING"
/bin/echo "" >> /tmp/$MYFILENAME
/bin/cat /tmp/$GREPRESULT >> /tmp/$MYFILENAME
else
WARNING_STATUS="OK"
/bin/echo "" >> /tmp/$MYFILENAME
/bin/echo "NO WARNING FOUND" >> /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME
fi
/bin/echo "Temp. notice: Check chkrootkit update manually " >> /tmp/$MYFILENAME
/bin/echo "================== END OF NOTICE ================" >> /tmp/$MYFILENAME
/bin/mail -s "[chkrootkit][$WARNING_STATUS|N] Check Rootkit Scan Report  @ $MYHOSTNAME" $MYEMAIL < /tmp/$MYFILENAME

/bin/rm -rf /tmp/$MYFILENAME
/bin/rm -rf /tmp/$GREPRESULT

/bin/echo "[chkrootkit] Scan Status: $WARNING_STATUS"
/bin/echo "[chkrootkit] Done checking system. Email is set to be sent to $MYEMAIL"
/bin/echo "======================================"
